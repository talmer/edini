// edini
// inilike files editing utility
package main

import (
	"flag"
	"fmt"

	"os"

	"github.com/go-ini/ini"
)

const (
	ActSet = "set"
	ActGet = "get"
)

var (
	fFileName = flag.String("f", "", "Cfg or Ini File Name")
	fSection  = flag.String("s", "", "Section name")
	fProperty = flag.String("p", "", "Property name")
	fValue    = flag.String("v", "", "Property value")
	fAction   = flag.String("a", ActSet, fmt.Sprintf("Action [ %s / %s ]", ActGet, ActSet))
)

func main() {
	// parsing args
	if !flag.Parsed() {
		flag.Parse()
	}

	// checking args
	if *fFileName == "" || *fProperty == "" {
		flag.Usage()
		os.Exit(1)
	}

	// loading file
	cfg, err := ini.Load(*fFileName)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	// taking actions
	if *fAction == ActSet {
		cfg.Section(*fSection).Key(*fProperty).SetValue(*fValue)
		if err := cfg.SaveTo(*fFileName); err != nil {
			fmt.Printf("Fail to save file: %v", err)
			os.Exit(1)
		}
	} else if *fAction == ActGet {
		fmt.Print(cfg.Section(*fSection).Key(*fProperty).String())
	} else {
		fmt.Printf("Wrong action %v", *fAction)
		os.Exit(1)
	}
}
